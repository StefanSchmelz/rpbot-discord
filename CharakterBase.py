#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import uuid


class CharakterBase:
    def __init__(self, data=None):
        if data:
            for key in data.keys():
                self.add_attribute(key, data[key])

    def add_attribute(self, name, value, invisible=False):
        name = name.replace(" ", "_")
        if not invisible:
            attr_name = "attribute_" + name
        else:
            attr_name = "attribute_inv_" + name
        setattr(self, attr_name, value)

    def get_visible_attributes(self):
        return [attribute for attribute in dir(self) if attribute.startswith("attribute_") and "attribute_inv_" not in attribute]

    def get_generators(self):
        return [generator for generator in dir(self) if generator.startswith("gen_")]

    def get_attribute_names(self):
        return [attr.replace("prompt_", "") for attr in self.get_prompts()]

    def get_prompts(self):
        return [generator for generator in dir(self) if generator.startswith("prompt_")]

    def asdict(self):
        result = {}
        for attribute in [item for item in dir(self) if item.startswith("attribute_")]:
            result[attribute.replace("attribute_", "")] = getattr(self, attribute)

    def gen_characterID(self, user):
        self.add_attribute("characterID", uuid.uuid4(), True)

    def __str__(self):
        result = "# Schülerakte von {}\n".format(getattr(self, "attribute_Spielername", "\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_"))
        for item in [item for item in self.get_visible_attributes() if "name" in item.lower()]:
            result += "  - __**{}**:__ {}\n".format(item.replace("attribute_", "").replace("_", " "), getattr(self, item, "? ? ?"))
        for item in [item for item in self.get_visible_attributes() if "name" not in item.lower()]:
            result += "  - __**{}**:__ {}\n".format(item.replace("attribute_", "").replace("_", " "), getattr(self, item, "? ? ?"))
        return result
