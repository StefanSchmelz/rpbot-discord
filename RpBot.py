#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import os
import sys
import discord
import random
import asyncio
import Charakter
import util


class RpBot(discord.Client):
    def __init__(self, loop=None, **options):
        super().__init__(loop=loop, options=options)
        self.lore = {}
        self.help_message = ""
        self.characters = {}
        self.waffenkammer = {}

        self.load_config_files()

        self.character_creation_tasks = []
        self.character_creation_service = self.loop.create_task(self.create_character())

    def load_config_files(self):
        try:
            with open(os.getenv("LORE_FILE"), "r") as file:
                self.lore = json.load(file)
            with open(os.getenv("MESSAGES_FILE"), "r") as file:
                data = json.load(file)
                self.help_message = data["bibliotek_hilfe"]
            with open(os.getenv("WEAPON_FILE")) as file:
                self.waffenkammer = json.load(file)
            with open(os.getenv("CHARACTER_FILE"), "r") as file:
                self.characters = json.load(file)
            with open(os.getenv("CHARQUEUE_FILE"), "r") as file:
                character_queue = json.load(file)
                self.character_creation_tasks = character_queue["chars"][:]
                print("Resuming the character creation")
        except FileNotFoundError:
            print("Cannot read a config file")

    async def on_ready(self):
        print(f'"{self.user.name}" has connected to Discord!')
        # await self.get_channel(701884165490081845).send("{} ist wieder online".format(self.user.name))

    async def create_character(self):
        await self.wait_until_ready()
        while True:
            if len(self.character_creation_tasks) > 0:
                user = self.get_user(self.character_creation_tasks[0])
                print("Starting character creation for {}".format(user.name))
                char = Charakter.Charakter()
                data = {}
                for item in [item for item in dir(user) if not item.startswith("_")]:
                    data[item] = getattr(user, item)
                for generator in char.get_generators():
                    sys.stdout.write(generator.replace("gen_", "") + "   ")
                    sys.stdout.flush()
                    getattr(char, generator, None)(data)

                await user.create_dm()
                await user.dm_channel.send(getattr(char, "message_start")())
                sheet_msg = await user.dm_channel.send(char)
                # print(char.get_prompts())
                for attribute in char.get_attribute_names():
                    sys.stdout.write(attribute + "   ")
                    sys.stdout.flush()
                    text = getattr(char, "prompt_" + attribute, None)()
                    if type(text) == str:
                        await user.dm_channel.send(text)
                    elif type(text) == list:
                        for part in text:
                            await user.dm_channel.send("\n".join(part))

                    def is_from_user(m):
                        return user.id == m.author.id
                    response = await self.wait_for("message", check=is_from_user)
                    print(response.content)
                    success, comment = getattr(char, "check_" + attribute, None)(response.content)
                    while not success:
                        await user.dm_channel.send(comment)
                        response = await self.wait_for("message", check=is_from_user)
                        print(response.content)
                        success, comment = getattr(char, "check_" + attribute, None)(response.content)
                    await user.dm_channel.send(comment)
                    await sheet_msg.edit(content=str(char))

                # await asyncio.sleep(20)
                print("Finishing the character creation for {}".format(self.get_user(self.character_creation_tasks[0]).name))
                self.characters[user.id] = char
                print(self.characters)
                print("Savig Characters")
                with open(os.getenv("CHARACTER_FILE"), "w") as file:
                    json.dump(self.characters, file, ensure_ascii=False, indent=4)
                print("Characters are saved!")

                await user.dm_channel.send("Damit sind wir fertig und ich mach den rest.")
                self.character_creation_tasks.remove(self.character_creation_tasks[0])

            else: # NO work to do
                await asyncio.sleep(1)

    async def on_member_join(self, member):
        print(f'"{member.name}" Has joined the server.')
        welcome_message = [f"Durch den großen, marmornen Torbogen tritt ein neuer Begabter. Sein Namensschild verrät, dass er {member.name} heißt.",
                           f"{member.name} bewundert den kleinen Wald im Innenhof der Akademie. Sowas hat {member.name} noch nie gesehen."]
        await self.get_channel(701884165490081845).send(random.choice(welcome_message))
        await member.create_dm()
        message = f'Hallo {member.name}, herzlich Willkommen an der kaiserlichen Akademie der arkanen Künste!'
        if len(self.character_creation_tasks) > 1:
            message += f"Wir füllen gleich deine Schülerakte aus. Leider sind vor dir noch {len(self.character_creation_tasks) - 1} andere dran."
        else:
            message += "Ich hole nur noch schnell einen neuen Vordruck."
        await member.dm_channel.send(message)
        self.character_creation_tasks.append(member.id)

    async def on_message(self, message):
        if message.author == self.user: #Ignore own messages
            return
        if message.content.startswith('!'):
            await self.parse_commands(message)
        elif message.content.startswith('%'):
            await self.parse_commands(message, True)
        else:
            pass
            # await message.author.create_dm()
            # await message.author.dm_channel.send(f'du hast folgendes geschrieben: {message.content}')
        # print(f'{message.author} sent: "{message.content}" in {message.channel}({message.channel.id})')

    async def parse_commands(self, cmd, private=False):
        # print(f'Command from {cmd.author} in {cmd.channel}: {cmd.content}')
        text = cmd.content[1:]
        text = text.split(" ")
        if text[0].lower() == "hilfe":
            await cmd.author.create_dm()
            await cmd.author.dm_channel.send("\n".join(self.help_message))
        if text[0].lower() == "bibliothekar" or text[0].lower() == "bib":
            if len(text) == 1:
                response = "Die Bibliothek:\n" + "\n".join(stringify_map(self.lore, 4, 4))
            elif len(text) > 3:
                if text[1].lower() == "neu":
                    path = text[2:text.index("->")]
                    eintrag = path[-1]
                    nest(self.lore, path[:-1])
                    tmp = self.lore
                    for item in path[:-1]:
                        tmp = tmp[item]
                    tmp[eintrag] = (" ".join(text[text.index("->") + 1:])).split("\n")
                    with open(os.getenv("LORE_FILE"), "w") as file:
                        json.dump(self.lore, file, sort_keys=True, indent=2, ensure_ascii=False)
                response = "Eintrag erstellt"
            else:
                if text[1] in self.lore.keys() and text[2] in self.lore[text[1]].keys():
                    response = "\n".join(self.lore[text[1]][text[2]])
                else:
                    response = "Dieser Eintrag existiert nicht!"
            if private or cmd.channel.type == discord.ChannelType.private:
                await cmd.author.create_dm()
                await cmd.author.dm_channel.send(response)
            elif cmd.channel.id in [701891098330529812, 701892103281573888, 701892213159755816, 701892188820209807, 701892149536489473] :
                await cmd.channel.send(response)

        if text[0] == "reload":
                self.load_config_files()
                await cmd.author.create_dm()
                await cmd.author.dm_channel.send("Okay")

        if text[0].lower() == "akte":
            if len(text) == 1:
                await cmd.author.create_dm()
                await cmd.author.dm_channel.send("Ich weiß nicht, was ich damit machen soll.")
            elif len(text) == 2:
                if text[1] == "erstellen":
                    if cmd.author.id not in self.character_creation_tasks:
                        self.insert_character_task(cmd.author.id)
                        message = f'Hallo {cmd.author.name}, herzlich Willkommen an der kaiserlichen Akademie der arkanen Künste!'
                        if len(self.character_creation_tasks) > 1:
                            message += f"Wir füllen gleich deine Schülerakte aus. Leider sind vor dir noch {len(self.character_creation_tasks) - 1} andere dran."
                        else:
                            message += "Ich hole nur noch schnell einen neuen Vordruck."
                        await cmd.author.create_dm()
                        await cmd.author.dm_channel.send(message)
                if text[1].lower() == "schlange":
                    await cmd.author.create_dm()
                    await cmd.author.dm_channel.send("Warteschlange:\n" + "\n".join([" - " + self.get_user(item).name for item in self.character_creation_tasks]))

            elif len(text) >= 3:
                if text[1] == "erstellen":
                    for member in self.guilds[0].members:
                        if member.name == " ".join(text[2:]):
                            self.insert_character_task(member.id)
                            await cmd.author.create_dm()
                            await cmd.author.dm_channel.send("Ok")
                            message = f'Hallo {member.name}, herzlich Willkommen an der kaiserlichen Akademie der arkanen Künste!'
                            if len(self.character_creation_tasks) > 1:
                                message += f"Wir füllen gleich deine Schülerakte aus. Leider sind vor dir noch {len(self.character_creation_tasks) - 1} andere dran."
                            else:
                                message += "Ich hole nur noch schnell einen neuen Vordruck."
                            await member.create_dm()
                            await member.dm_channel.send(message)
                            break
                    else:
                        await cmd.author.create_dm()
                        await cmd.author.dm_channel.send("Nope")


    # def on_error(self, event_method, *args, **kwargs):
    #     print("\x1B[31m" + event_method + "\x1B[0m")
    #     print("\x1B[31m" + args + "\x1B[0m")

    def insert_character_task(self, userid):
        print("Queueing Charakter creation for {}".format(self.get_user(userid)))
        self.character_creation_tasks.append(userid)
        with open(os.getenv("CHARQUEUE_FILE"), "w") as file:
            json.dump({"chars": self.character_creation_tasks[:]}, file, ensure_ascii=False, indent=4)


def stringify_map(data, indent, width):
    result = []
    if type(data) is dict:
        for key in data.keys():
            result.append((" " * indent) + "- " + key)
            if data[key]:
               result.extend(stringify_map(data[key], indent + width, width))
    return result


def nest(data, keys):
    tmp = data
    for key in keys:
        if key not in tmp.keys():
            tmp[key] = {}
        tmp = tmp[key]
