#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import os
import sys

import util
import CharakterBase
import random
import dotenv


class Charakter(CharakterBase.CharakterBase):
    TemperatuResistenzen = {"mensch": 1.5, "elf": 3, "elfe": 3, "halbelf": 2.75, "zwerg": 3, "halbzwerg": 2.75,
                            "neko": 1.5, "kemonomimi": 1.5, "halbdrache": 3, "vampir": 5, "werwolf": 4, "furry": 2.5,
                            "fee": 12}
    Körpertemperaturen = {"mensch": 37, "elf": 35, "elfe": 35, "halbelf": 36, "zwerg": 40, "halbzwerg": 38,
                            "neko": 38, "kemonomimi": 37, "halbdrache": 60, "vampir": 20, "werwolf": 39, "furry": 37,
                            "fee": 32}
    def __init__(self, data={}):
        super().__init__(data=data)

    def message_start(self):
        result = "\n".join(["Herzlich Willkommen {},".format(self.attribute_Spielername),
                            "Bevor ich dich alleine auf dem Gelände rumlaufen lassen kann, müssen wir deine Schülerakte ausfüllen.",
                            "Am besten jetzt sofort!",
                            "Bei allen fragen gilt immer, es genügt, wenn du mir nur die info gibst, nach der ich gefragt habe.(Dann muss ich weniger denken 😉)",
                            "Wenn du dich vertippt hast, ist das nicht schlimm! Dann sag mir einfach am ende dass du was ändern willst :-)",
                            "Wenn du mir über etwas keine Auskunft geben willst, dann kannst du mit `überspringen` antworten.",
                            "Immer wenn ich von deinem alten Leben rede - oder dich Mit deinem Namen Anspreche, dann meine ich dich vor dem Bildschirm und ansonsten meine ich deinen Charakter."])
        return result

    def gen_Spielername(self, user):
        self.add_attribute("Spielername", user["name"][:])

    def gen_discordID(self, user):
        self.add_attribute("discordID", user["id"], True)

    def gen_Ausdauer(self, user):
        self.add_attribute("Ausdauer", 10 + random.randint(1,10), True)

    def prompt_01_Spieleralter(self):
        text = "\n".join([
            f"Soo {self.attribute_Spielername} wir machen jetzt am besten mit deinem richtigen Alter weiter! Ich hätte gern mindestens **16** Jahre, sonst darf ich dich leider nicht reinlassen.",
            "Ich kann dich die Frage nicht überspringen lassen! Ohne dass du mir die Frage beantwortest kann ich dich nicht rein lassen."
        ])
        return text

    def check_01_Spieleralter(self, text):
        nums = [int(s) for s in text.split() if s.isdigit()]
        if hasattr(self, "attribute_Spieleralter") and text.lower() == "ja":
            return True, "Ich will es für dich hoffen"
        elif hasattr(self, "attribute_Spieleralter"):
            return True, "Ich frag lieber nicht groß weiter."
        try:
            if nums[0] < 16:
                return False, "Es tut mir sehr leid, aber du darfst nicht rein!\nBitte verlasse den server."
            else:
                self.add_attribute("Spieleralter", int(text))
                return False, "So jung wär ich auch gern mal wieder! Aber stimmt das denn auch wirklich?"
        except ValueError:
            return False, "Das klingt nicht nach einem Alter.\n Ich glaube du willst mich verarschen!\nDenk lieber nochmal drüber nach, bevor du mir erneut antwortest!"

    def prompt_02_Spielergeschlecht(self):
        return "Sooooo {} warst du in deinem alten Leben Männlich oder weiblich?".format(
            self.attribute_Spielername)

    def check_02_Spielergeschlecht(self, text):
        if text.lower() in ["junge", "mann", "männlich", "männchen", "m"]:
            self.add_attribute("Spielergeschlecht", "männlich")
            return True, "Dann kommst du mit den anderen Jungs in den Kerker.\nIhr macht doch eh so viel unsinn, dass ihr dauernd unten sein werdet!"
        elif text.lower() in ["mädchen", "mädel", "mädl", "madl", "frau", "frauchen", "weiblich", "weibchen"]:
            self.add_attribute("Spielergeschlecht", "weiblich")
            return True, "Pass schön in den Schlafsäälen und Zimmern auf, die Jungs spannen gern und mit allem was ihnen einfällt."
        else:
            return False, "Das ist kein mir bekanntes Geschlecht!\nMuss wohl sowas wie \"Non-binary\" sein...\nSuch dir ein normales aus!"

    def prompt_03_Charaktername(self):
        return "Und was ist dein neuer Name unter dem du die Schule besuchen willst?"

    def check_03_Charaktername(self, text):
        self.add_attribute("Charaktername", text)
        return True, "Okay {}, dann nenne ich dich ab jetzt einfach: {}".format(self.attribute_Spielername,
                                                                                self.attribute_Charaktername)

    def prompt_04_Charakteralter(self):
        return "Zuerst interessiert mich ja, wie alt dein Charakter ist und in welche Schulklasse er kommt."

    def check_04_Charakteralter(self, text):
        nums = [int(s) for s in text.split() if s.isdigit()]
        try:
            if nums[0] < 7:
                return False, "Naja, ich weiß dass zu schätzen, dass du schon soo früh in die Schule willst, aber du bist trotzdem zu Jung!\nKomm wieder, wenn du **7** oder älter bist."
            elif nums[0] > 18:
                self.add_attribute("Charakteralter", int(text))
                return True, "Zum glück für dich kann man an der Akademie auch Studieren, sonst hätte ich dich wofort weider Rausgeworfen!"
            elif nums[0] == 7:
                self.add_attribute("Charakteralter", int(text))
                self.add_attribute("Schulklasse", 1)
                return True, "Okay, dann viel Spaß in der ersten Klasse."
            else:
                self.add_attribute("Charakteralter", int(text))
                self.add_attribute("Schulklasse", 1)
                return True, "Okay, aber du fängst trotzdem als Erstklässler an!"
        except ValueError:
            return False, "Das klingt nicht nach einem Sinnvollen Alter!\nDenk lieber nochmal nach"

    def prompt_05_Charaktergeschlecht(self):
        return "Bist du in hier an der Akademie auch {}(Ja, Nein)".format(self.attribute_Spielergeschlecht)

    def check_05_Charaktergeschlecht(self, text):
        result = ja_nein(text)
        if type(result) is not None:
            if text.lower():
                self.add_attribute("Charaktergeschlecht", self.attribute_Spielergeschlecht)
                return True, "Ist ja langweilig"
            else:
                if self.attribute_Spielergeschlecht == "männlich":
                    self.add_attribute("Charaktergeschlecht", "weiblich")
                else:
                    self.add_attribute("Charaktergeschlecht", "männlich")
                return True, "Interessant"
        else:
            return False, "Das war eine Ja-Nein-Frage!"

    def prompt_06_Uniform(self):
        text = [[
            "Hier ein Auszug aus der Schulordnung der Akademie:\n```\nEs gibt fünf unterschiedliche Uniformen an dieser Akademie.",
            "Manche Rassen dieser Akademie fühlen sich mit ein wenig weniger Stoff am Körper wohler, weswegen es auch immer eine freizügige Variante.",
            "Schüler, die diese Regeln nicht einhalten, wenn sei zum Unterrischt erscheinen, werden von der Stunde ausgeschlosssen.",
            "Männlich regulär:",
            "   Die normale Uniform für männliche Schüler ist ein marineblauer, knöchellanger Umhang mit goldenen Knöpfen und einer weiten Kapuze.",
            "   Darunter ist ein weißes Hemd mit ebenfalls goldenen Knöpfen zu tragen.",
            "   Je nach Geschmack des Schülers darf eine graue oder marineblaue Veste über dem Hemd getragen werden - diese ist allerdings kein Muss.",
            "   Dazu wird eine marineblaue, lange Hose und dunkle Anzugschuhe ohne auffällige Strukturen gefordert.",
            "Männlich freizügig:",
            "   Die freizügige Uniform für männliche Schüler ist ein marineblauer, knöchellanger Umhang mit goldenen Knöpfen und einer weiten Kapuze.",
            "   Darunter sind mindestens zu tragen: marineblaue Shorts mit goldenen Nähren und knöpfen, einfache, schwarze Schuhe.\n```"],
            ["```\nWeiblich regulär:",
             "   Die normale Uniform für weibliche Schüler ist ein marineblauer, knöchellanger Umhang mit goldenen Knöpfen und einer weiten Kapuze.",
             "   Darunter ist eine weiße Bluse mit ebenfalls goldenen Knöpfen zu tragen.",
             "   Des weiteren sind zu tragen: ein Mindestens knielanger, marineblauer Rock, Dunkle Strümpfe und flache, schwarze Schuhe.",
             "Weiblich freizügig:",
             "   Die normale Uniform für weibliche Schüler ist ein marineblauer, knöchellanger Umhang mit goldenen Knöpfen und einer weiten Kapuze.",
             "   Es ist - um übermäßige Ablenkung der männlichen Mitschüler mindestens folgendes zu tragen.",
             "   - Ein marineblauer - optional trägerloser Büstier mit goldenen Nähten. Es wird darum gebeten von anregenden Ausführungen abzusehen.",
             "   - Ein marineblauer Rock, welcher minrestens die hälfte des Oberschenkels bedeckt und goldene Akzente besitzt.",
             "     Alternativ ist auch eine beinlose, anliegende Hose mit goldenen Nähten zugelassen.",
             "   - Je nach Vorliebe schwarze Strümpfe und flache, schwarze Schuhe.",
             "Weiblich klassisch",
             "   Die normale Uniform für weibliche Schüler ist ein marineblauer, knöchellanger Umhang mit goldenen Knöpfen und einer weiten Kapuze. Darunter ist ein weißes",
             "   - mindestens knielanges - Kleid mit einem marineblauen Gürtel und goldener Schnalle zu tragen.",
             "   Bei dieser uniform sind weiße Strümpfe und dunke, flache Schuhe pflicht.",
             "\nJede Variante kann ggf. von der Akademie bezogen werden.\n```\nMir genügt es, wenn du jeweils nur den Anfangsbuchstaben von geschlecht und Variante.",
             "Ansonsten schreib einfach die beides aus."]]
        return text

    def check_06_Uniform(self, text):
        if len(text) < 4:
            text = text.replace(" ", "")
            if len(text) == 2:
                if text[0].lower() not in "mw":
                    return False, "Geschlechter mit solchen kürzeln kenne ich nicht."
                if text[1].lower() not in "rfk":
                    return False, "Oh, eine neue Variante von Uniform...\nDie kann ich dir nicht erlauben"
                if text.lower() == "mk":
                    return False, "Es git für jungs keine Klassische Uniform!"
                if text[0] == "m":
                    res = "männlich "
                else:
                    res = "weiblich "
                if text[1] == "r":
                    res += "regulär"
                elif text[1] == "f":
                    res += "freizügig"
                else:
                    res += "klassisch"
                self.add_attribute("Uniform", res)
                return True, "Naja, ich bezweifel immernoch, dass es dir steht."
            else:
                return False, "das klingt irgendwie falsch."
        else:
            worte = text.lower().split(" ")
            if len(worte) != 2:
                return False, "Ich glaub du solltest nochmal in Ruhe überlegen..."
            if worte[0] not in ["männlich", "weiblich"]:
                return False, "Uniformen für dieses Geschlecht existieren nicht!\nProbiers nochmal!"
            if worte[1] not in ["regulär", "freizügig", "klassisch"]:
                return False, "Die Variante \"{}\" kenn ich garnicht".format(worte[1])
            if worte[0] == "männlich" and worte[1] == "klassisch":
                return False, "Ich bin mir nicht sicher, ob die Schulordnung aktuell ist, aber in dieser Ausgabe steht keine klassische Uniform Für Jungen!"
            self.add_attribute("Uniform", worte[0] + " " + worte[1])
            if self.attribute_Charaktergeschlecht != worte[0]:
                return True, "Na gut, auch wenn ich glaube du wirst darin sehr lächerlich aussehen."
            else:
                return True, "Bin mir nur nicht sicher, ob dir das steht..."

    def prompt_07_Outfits(self):
        return "Im Namen des Diziplinarkomitees der Akademie muss ich dich nach deiner Freizeitkleidung fragen.\n" + \
               "beschreib am besten kurz typische Outfits, die du In deiner Freizeit anhast, wenn du Sport machst, in der Bibliothek lernst oder einfach nur im Gemeinschaftsraum rumhängst.\n" + \
               "Schreib einfach nach dem Beispiel:\n" + \
               "Bürokratie:" + \
               "Einfacher, grauer, Overoll mit eienm gürtel für die Peitschen.\n" + \
               "Putzen:\nEinfacher, grauer Kittel mit Peitschen am Gürtel\nIch werd das aber einfach mal so übernehmen."

    def check_07_Outfits(self, text):
        outfits = extract_paragraph(text)
        if len(outfits) > 0:
            self.add_attribute("Outfits", outfits)
            return True, "Das trägt man also heute...."
        else:
            return False, "Damit kann ich nichts anfangen!\nBitte achte darauf, dass du dich an das Beispiel hälst!"

    def prompt_08_Aussehen(self):
        return "\n".join(["Als nächstes ein Paar fragen zu deinem Aussehen. Das ist im Namen der Fotografengilde.",
                          "Welche Augenfarbe hast du?"])

    def check_08_Aussehen(self, text):
        if not hasattr(self, "attribute_Aussehen"):
            self.add_attribute("Aussehen", {"augenfarbe": text, "haarfarbe": ""})
            return False, "Als nächstes die Haarfarbe:"
        else:
            aussehen = self.attribute_Aussehen
            if aussehen["haarfarbe"] == "":
                aussehen["haarfarbe"] = text
                aussehen["frisur"] = ""
                return False, "Als nächstes: Wie ist deine Frisur?"
            elif aussehen["frisur"] == "":
                aussehen["frisur"] = text
                if self.attribute_Charaktergeschlecht == "männlich":
                    aussehen["gesichtsbehaarung"] = ""
                    return False, "Hast du einen Bart und wenn Ja wie sieht er aus?"
                else:
                    aussehen["gesichtsbeschreibung"] = ""
                    return False, "Gibt es sonst nochwas über dein Gesicht zu sagen?"
            elif ("gesichtsbehaarung" in aussehen.keys()) and aussehen["gesichtsbehaarung"] == "":
                if text not in ["keiner", "nein"]:
                    aussehen["gesichtsbehaarung"] = text
                else:
                    del aussehen["gesichtsbehaarung"]
                aussehen["gesichtsbeschreibung"] = ""
                return False, "Gibt es sonst nochwas über dein Gesicht zu sagen?"
            elif ("gesichtsbeschreibung" in aussehen.keys()) and aussehen["gesichtsbeschreibung"] == "":
                if text not in ["nichts", "nein"]:
                    aussehen["gesichtsbeschreibung"] = text
                else:
                    del aussehen["gesichtsbeschreibung"]
                aussehen["größe"] = ""
                return False, "Wie groß bist du?(bitte in cm angeben)"
            elif aussehen["größe"] == "":
                nums = [int(i) for i in text.split() if i.isdigit()]
                if len(nums) > 0:
                    aussehen["größe"] = nums[0] / 100
                    aussehen["figur"] = ""
                    return False, "Als nächstes deine Figur:"
                else:
                    return False, "Das ist für mich keine größe. "
            elif aussehen["figur"] == "":
                aussehen["figur"] = text
                aussehen["merkmale"] = ""
                return False, "Als nächstes: Hast du irgendwelche besonderen Merkmale wie Muttermale, Tattoos oder narben?\nDer Schmuck bekommt gleich ein extra feld."
            elif ("merkmale" in aussehen.keys()) and aussehen["merkmale"] == "":
                if text not in ["keine", "nein"]:
                    aussehen["merkmale"] = text
                else:
                    del aussehen["merkmale"]
                aussehen["schmuck"] = ""
                return False, "Jetzt Bitte als nächstes dein Schmuck:"
            else:
                if text not in ["keiner", "nein"]:
                    aussehen["schmuck"] = text
                else:
                    del aussehen["schmuck"]
                return True, "das wars mit deinem Aussehen."

    def prompt_Waffe(self):
        return "\n".join([
            "Dann machen wir am besten sofort mit der Frage weiter, welche Waffe dein Charakter wählt.",
            "Jeder Begabte hat eine magische Waffe. Wenn du mir `waffenkammer` schreibst, kann ich dir auch unsere Waffenkammer zeigen.",
            "Mir genügt es wenn du nur die art von Waffe nennst.",
            "(Die Beschreibung kommt danach 😉, wenn du dir nichts aus der Waffenkammer aussuchst.)"])

    def check_Waffe(self, text):
        dotenv.load_dotenv()
        waffenkammer = json.load(open(os.getenv("WEAPON_FILE"), "r"))
        if hasattr(self, "attribute_Waffe"):
            self.add_attribute("Waffe_beschreibung", text, True)
            return True, "Schön Schön, Kann es kaum erwarten dir das abzunehmen!"

        if text.lower() == "waffenkammer":
            return False, "Unsere Waffen:\n" + "\n".join(util.print_map(waffenkammer, 4,
                                                                        4)) + "\nWas willst du denn jetzt?\n(Hier zählt ausnahmsweise mal die Groß- und Kleinschreibung!)"
        else:
            weapons = []
            for category in waffenkammer.keys():
                for item in waffenkammer[category].keys():
                    weapons.append(item)
            if text in weapons:
                self.add_attribute("Waffe", text)
                return True, "Okay"
            else:
                self.add_attribute("Waffe", text)
                return False, "Zumindest eine kleine Beschreibung brauch ich doch noch."

    def prompt_Haus(self):
        return "\n".join([ # FIXME: Bessere Beschreibungen
            "gören wie du haben sich wahrscheinlich noch nicht mit der Schulordnung beschäftigt, deswegen ist hier ein Auszug aus dieser:\n\n"
            "Es gibt an dieser Akademie vier Häuser in die die Schüler eingeteilt werden.",
            "Jedes besitzt ein eigenes Wohnheim und eine eigene Bibliothek.",
            "Ich werde dir ein wenig über die Häuser erzählen müssen, damit du dich entscheiden kannst:",
            "",
            " - Richwin: Unter den Schülern wird sich erzählt, dass der Kaiser selbst darauf bestanden hat, dass dieses Haus eingerichtet wird.",
            "   Es sollte sicherstellen, dass das Reich immer Begabte hat, die seinen Platz einnehmen sollten, wenn ihm etwas passiert.",
            "   Deswegen legt dieses Haus großen Wert auf Eigenschaften wie zum Beispiel Weisheit, Gerechtigkeit.",
            "",
            " - Sisgard: Einer der Mitgründer der Akademie war des Kaisers bester General. Er bestand darauf,\ndass es im Reich einen Bestand von Begabten geben muss,",
            "   die in der Lage wären ihr Land und ihren Herrn verteidigen zu könnten.",
            "   Deswegen legt dieses Haus sehr viel Wert auf Mut und Tureue.",
            "",
            " - Brandolf: Einig indes war man sich darüber, dass Begabte die Städte bewachen sollten und so wurde das Haus der Wächter geschaffen.",
            "   Dieses Haus Legt großen wert darauf, dass seine Bewohner Sind wie Felsen. Standfest, dass kein Sterblicher sie überwinden kann.",
            "   Und ausdauernd wie ein Berg, dass sie ihren Platz niemals verlassen, wenn sie bleiben sollen.",
            "",
            " - Tilrun: Die Feinde sollten zittern vor den Geistern des Reiches, deswegen Legt das Haus der Jäger großen wert darauf,",
            "   dass seine Bewohner in der Lage sind Ruhig zu warten, bis sich die Perfekte Gelegenheit zum Zuschlagen bietet.",
            "   Sie sollen so präzise zuschlagen, dass Nur ihre Beute mitbekommt dass sie angegriffen wird."
        ])

    def check_Haus(self, text):
        if text.lower() in ["richwin", "sisgard", "brandolf", "tilrun"]:
            self.add_attribute("Haus", text[0].upper() + text[1:].lower())
            return True, "{} willst also ein {} sein?\nWir werden sehen, ob du dem Haus würdig bist...".format(
                self.attribute_Charaktername, self.attribute_Haus)
        else:
            return False, "Ich glaub dieses Haus haben wir nicht...\nVersuchs nochmal!"

    def prompt_Rasse(self):
        if self.attribute_Charaktergeschlecht == "männlich":
            return "\n".join([
                "Damit wir uns besser um dich und deine Bedürfnisse kümmern können, solltest du uns noch sagen, Welcher der folgenden Rassen du dich angehörig fühlst.",
                " - Menschen\n - Elfen\n - Halbelfen\n - Zwerge\n - Halbzwerge\n - Neko\n - Kemonomimi(Menschen mit Öhrchen und schwanz)",
                " - Halbdrachen\n - Vampire\n - Werwölfe\n - Diverse Furries\n - Feen",
                "Also was bist du?",
                "Antworte bitte nach folgendem Beispiel: `Ich bin ein <Hier beliebige Rasse einfügen>`"])
        else:
            return "\n".join([
                "Damit wir uns besser um dich und deine Bedürfnisse kümmern können, solltest du uns noch sagen, Welcher der folgenden Rassen du dich angehörig fühlst.",
                " - Menschen\n - Elfen\n - Halbelfen\n - Zwerge\n - Halbzwerge\n - Neko\n - Kemonomimi(Menschen mit Öhrchen und schwanz)",
                " - Halbdrachen\n - Vampire\n - Werwölfe\n - Diverse Furries\n - Feen",
                "Also was bist du?",
                "Antworte bitte nach folgendem Beispiel: `Ich bin eine <Hier beliebige Rasse einfügen>`"])

    def check_Rasse(self, text):
        for item in ["mensch", "elf", "elfe", "halbelf", "zwerg", "halbzwerg", "neko", "kemonomimi",
                                "halbdrache", "vampir", "werwolf", "furry", "fee"]:
            if item  in text.lower():
                self.add_attribute("Rasse", worte[3])
                self.add_attribute("Temperaturresistenz", Charakter.TemperatuResistenzen[worte[3]], True)
                self.add_attribute("Körpertemperatur", Charakter.Körpertemperaturen[worte[3]], True)
                return True, "Nicht noch ein {}".format(worte[3])
            else:
                return False, "Was sagst du? Rede bitte deutlicher! Ich will eins der Folgenden Worte hören:Mensch, Elf, Elfe, Halbelf, Zwerg, Halbzwerg, Neko, Kemonomimi, Halbdrache, Vampir, Werwolf, Furry, Fee"
        else:
            return False, "Ich glaube nicht, dass wir diese Rasse schon kennen. Schlag sie doch unseren Admins vor."

    def prompt_Haustier(self):
        return "\n".join(["In den Kreisen der Akademie haben viele Schüler Haustiere dabei.",
                   "Die Eulen, Katzen, Falken, Raben, Ratten, Mäusen, Spinnen und Kröten sind zumeist selbst magisch begabt.",
                   "Die Haustiere reledigen meist botengänge und liefern nachrichten oder kleinere Päckchen aus.",
                   "Als Haustier kann fast jedes kleinere Tier benutzt werden.",
                   "Ich möchte jetzt nur wissen, ob du eins hast.",
                   "Wenn dein Haustier ein bisschen größer ist und auch hier zur schule geht ist das ebenfalls zulässig. \uD83D\uDE09",
                   "Hast du ein Haustier?"])

    def check_Haustier(self, text):
        if hasattr(self, "attribute_Haustier"):
            haustier = self.attribute_Haustier
            if haustier["vorhanden"]:
                if haustier["name"] == "":
                    haustier["name"] = text
                    haustier["art"] = ""
                    return False, "Als nächstes wüsste ich gerne die Tierart von deinem Haustier"
                elif haustier["art"] == "":
                    haustier["art"] = text
                    haustier["rasse"] = ""
                    return False, "Nicht noch so ein Ding bitte...\nNaja, als nächstes brauch ich die Rasse von dem Biest."
                elif haustier["rasse"] == "":
                    haustier["rasse"] = text
                    haustier["beschreibung"] = ""
                    return False, "Fast fetig mit dem Ding. Du musst mir nur noch kurz erzählen, wie das Biest aussieht."
                elif haustier["beschreibung"] == "":
                    haustier["beschreibung"] = text
                    return True, "Endlich ein anderes Thema....\nIch hasse Fiecher!"
            else:
                return True, "Gut, dann muss ich nicht hinter nocheinem stinkenden, mistFiech herputzen...."
        else:
            if "j" in text.lower():
                self.add_attribute("Haustier", {"vorhanden": True, "name": ""})
                return False, "Als nächstes wüsste ich ganz gerne, wie dein Haustier heißt."
            elif "n" in text.lower():
                self.add_attribute("Haustier", {"vorhanden": False})
                return True, "Schade"

    def prompt_Position(self):
        return "".join(["Als nächstes Wüsste ich ganz gern, Ob du Schüler, Lehrer oder Angestellter der Akademie bist.",
                        "Es gibt folgende Positionen: Schüler, Lehrer, Angestellter, Hausgeist, Hausmädchen, Hausmeister",
                        "Schüler, Angestellter, oder Hausmädchen darfst du dir selbst aussuchen, aber bei den anderen halte bitte Rücksprache mit einem der Admins."])

    def check_Position(self, text):
        if text.lower() in ["schüler", "lehrer", "angestellter", "hausgeist", "hausmädchen", "hausmeister"]:
            self.add_attribute("Position", text[0].upper() + text[1:].lower())
            if text.lower() == "hausmeister":
                return True, "Toll, endlich bekomme ich verstärkung! 😍"
            else:
                return True, "Okay, dann wäre das geklärt.\nWeiter im Text"
        else:
            return False, "Den Berug kennen wir hier nicht!\nProbiers nochmal"

    def prompt_Dominanz(self):
        return "\n".join(["Wie Dominant bist du?",
                          "Also hast du lieber die Kontrolle, wirst du lieber kontolliert oder ist das mal so und mal so?",
                          "- \"Ich hab gerne die Kontrolle\" nennt man Dominant",
                          "- \"Ich geb gerne die Kontrolle ab\" nennt man Unterwürfig oder Submissiv"
                          "- \"Mal so und ma so\" nennt man Switch",
                          "Mir genügt dom, sub oder switch."])

    def check_Dominanz(self, text):
        if text.lower() in ["dom", "dominant"] or ("dom" in text.lower()):
            self.add_attribute("Dominanz", "dom")
            return True, "Dann ist das ja geklärt."
        elif text.lower() in ["sub", "submissiv", "unterwürfig"] or ("sub" in text.lower()):
            self.add_attribute("Dominanz", "sub")
            return True, "Bitte nicht noch ein Stiefellecker..."
        elif text.lower() == "switch":
            self.add_attribute("Dominanz", "switch")
            return True, "Jedem das seine"
        else:
            return False, "Ich galube nicht, dass uns das groß weiter führt. Es gibt nur drei versionen"

    def prompt_Begabung(self):
        return "\n".join(["Was ist deine Persönliche magische Begabung?",
                          "Bitte pass auf, dass du mindestens eine Einschränkung der Begabung für jeden größeren Vorteil hast.",
                          "Und bitte achte darauf, dass deine Begabung nicht zu mächtig ist, ansonsten ist es für die anderen doof.",
                          "Sei doch einfach creativ \uD83D\uDE43",
                          "Am meisten Spaß macht es für alle, wenn du dir was richtig verrücktes ausdenkst, worüber man lachen kann und was viel potential für witzige situationen bietet"
                          ])

    def check_Begabung(self, text):
        self.add_attribute("Begabung", text)
        return True, "Ich nehm das mal so hin. ich kann es ja nicht überprüfen."

    def prompt_Familie(self):
        return "\n".join(["Erzähl mir was über deine Familie.\nIst eine Mutter eine Begabte?"])

    def check_Familie(self, text):
        if not hasattr(self, "attribute_Familie"):
            result = ja_nein(text)
            if type(result) is not None:
                self.add_attribute("Familie", {"mutter-begabung": result, "mutter-lebendig": ""})
            else:
                return False, "Das war eine Ja/Nein-Frage....\nVersuch es nochmal!"
            return False, "Weiter damit ob deine Mutter noch am leben ist"
        else:
            familie = self.attribute_Familie
            if familie["mutter-lebendig"] == "":
                result = ja_nein(text)
                if type(result) is not None:
                    familie["mutter-lebendig"] = result
                else:
                    return False, "Das war eine Ja/Nein-Frage....\nVersuch es nochmal!"
                familie["vater-begabung"] = ""
                return False, "War dein vater ein Begabter?"
            elif familie["vater-begabung"] == "":
                result = ja_nein(text)
                if type(result) is not None:
                    familie["vater-begabung"] = result
                else:
                    return False, "Das war eine Ja/Nein-Frage....\nVersuch es nochmal!"
                familie["vater-lebendig"] = ""
                return False, "Ist dein Vater noch am Leben?"
            elif familie["vater-lebendig"] == "":
                result = ja_nein(text)
                if type(result) is not None:
                    familie["vater-lebendig"] = result
                else:
                    return False, "Das war eine Ja/Nein-Frage....\nVersuch es nochmal!"
                familie["geschichte"] = ""
                return False, "Die Fragen bisher waren nur um deine Abstammung ersten Grades zu bestimmen.\nJetzt hätte ich noch gern eine kurze Geschichte über deine Familie."
            else:
                familie["geschichte"] = text
                self.evaluate_heriage()
                return True, "Das war jetzt viel Familie"

    def prompt_Magie(self):
        return "\n".join(["Jetzt zu deiner Meinung zur Magie\nMagst du Magie?(Ja/nein)"])

    def check_Magie(self, text):
        if hasattr(self, "attribute_Magie"):
            zuneigung = self.attribute_Magie["zuneigung"]
            try:
                stärke = int(text)
            except ValueError:
                return False, "Das ist keine Zahl die ich verstehe!"
            if stärke > 10:
                return False, "Hier sind nur 10 Kästchen\nNochmal bitte😠"
            if stärke <= 3:
                result = "ein wenig "
            elif stärke <= 6:
                result = "moderat "
            else:
                result = "sehr "
            if zuneigung:
                result += "Zugetan"
            else:
                result += "abgeneigt"
            self.attribute_Magie = result
            return True, "Noch eine Seite fertig"
        else:
            if "j" in text:
                self.add_attribute("Magie", {"zuneigung": True})
            elif "n" in text:
                self.add_attribute("Magie", {"zuneigung": True})
            else:
                return False, "Nur ja und nein!\nNOCHMAL!"
            if self.attribute_Magie["zuneigung"]:
                return False, "Wie sehr bitst du der Magie zugetan?\nAm besten auf einer Skala von 0-10"
            else:
                return False, "Wie sehr bist du der Magie abgeneigt?\nAm besten auf einer Skala von 0-10"

    def prompt_Trauma(self):
        return "\n".join(["Viele Begabte haben in ihrem meist sehr jungen Leben schon mindestens ein Trauma erlebt",
                          "Erzähl mir von deinem."])

    def check_Trauma(self, text):
        self.add_attribute("Trauma", text)
        return True, "Grauenhaft, aber solche Geschichten hör ich jeden Tag."

    def prompt_Persönlichkeit(self):
        return "\n".join(["Deine Persönlickeit ist der Schulleitung natürlich wichtig",
                          "Jetzt sollst du dich selbst einschätzen."])

    def check_Persönlichkeit(self, text):
        self.add_attribute("Persönlichkeit", text)
        return True, "Wenigstens kannst du hier nichts falschmachen"

    def prompt_Vorlieben(self):
        return "\n".join(["Wer zum dreieinigen FeuerVogel auch immer dieses Feld in das Formular geschmuggelt hat, der war bestimmt ein Perverser, aber es hilft nix....",
                          "Hast du irgendwelche Vorlieben im Bett?"])

    def check_Vorlieben(self, text):
        if not hasattr(self, "attribute_Vorlieben"):
            if "j" in text:
                self.add_attribute("Vorlieben", "")
                return False, "Was sind das für welche?"
            elif "n" in text:
                self.add_attribute("Vorlieben", "keine")
                return True, "Gut, ich würdde es hier auch nicht angeben, wenn ich soeas hätte"
            else:
                return False, "Nur ja und nein!\nNOCHMAL!"
        else:
            self.attribute_Vorlieben = text
            return True, "Na du bist ja versaut..."

    def prompt_Tabus(self):
        return "\n".join(["Gibt es irgendwas was Für dich Tabu ist?",
                          "Es ist nicht nur nach Tabus im Bett gefragt."])

    def check_Tabus(self, text):
        if not hasattr(self, "attribute_Tabus"):
            if "j" in text:
                self.add_attribute("Tabus", "")
                return False, "Was sind das für welche?"
            elif "n" in text:
                self.add_attribute("Tabus", "keine")
                return True, "DAnn hast du es bisher echt gut gehabt."
            else:
                return False, "Nur ja und nein!\nNOCHMAL!"
        else:
            self.attribute_Vorlieben = text
            return True, "Hmmm"

    def prompt_Mag_ich(self):
        return "\n".join(["Was magst du gerne?", "Zum Beispiel Lieblingsessen, Lieblingsmusik und so weiter."])

    def check_Mag_ich(self, text):
        self.add_attribute("Mag ich", text)
        return True, "Okay"

    def prompt_Mag_ich_nicht(self):
        return "\n".join(["Jetzt ein paar Sachen, die du nicht magst.", "zum beispiel Essen, das du nicht magst"])

    def check_Mag_ich_nicht(self, text):
        self.add_attribute("Mag ich nicht", text)
        return True, "Interessant"

    def prompt_Sozialstatus(self):
        return "\n".join([
            "Wie bist du im Umgang mit anderen Menschen?",
            "Bist du eher herdentier oder Außenseiter?",
            "wie schnell kannst du dich in eine Gruppe einfinden und so weiter."])

    def check_Sozialstatus(self, text):
        self.add_attribute("Sozialstatus", text)
        return True, "Okay"

    def prompt_Meister(self):
        print("Meister Text")
        return "\n".join([
            "Hast du einen Meister hier an der Akademie?",
            "Wenn ja schreib seinen Namen hier rein.",
            "Wenn du mehrere Meister hast ist das acuh okay, dann schreib sie mit Kommas getrennt auf.",
            "Du kannst hier auch Jemanden reinschreiben, den du nur sehr bewunderst und dessen diener du gerne wärst :-D"])

    def check_Meister(self, text):
        self.add_attribute("Meister", text)
        return True, "Okay"

    def prompt_Diener(self):
        return "\n".join([
            "Hast du einen Untergebenen mit in die Akademie gebracht, oder erst hier einen Diener Eingestellt?",
            "Wenn du keinen Diener hast, dann kannst du die Frage einfach überspringen."
        ])

    def check_Diener(self, text):
        self.add_attribute("Diener", text)
        return True, "Okay"

    def evaluate_heriage(self):
        familie = self.attribute_Familie
        if familie["mutter-begabung"] and familie["vater-begabung"]:
            abstammung = "reinblut"
        elif (familie["mutter-begabung"] and not familie["vater-begabung"]) or (
                not familie["mutter-begabung"] and familie["vater-begabung"]):
            abstammung = "halbblut"
        else:
            abstammung = "jungblut"
        abstammung += " "
        if familie["mutter-lebendig"] and familie["vater-lebendig"]:
            abstammung += "Filus"
        elif familie["mutter-lebendig"] and not familie["vater-lebendig"]:
            abstammung += "Halbweise väterlich"
        elif not familie["mutter-lebendig"] and familie["vater-lebendig"]:
            abstammung += "Halbweise mütterlich"
        else:
            abstammung += "Vollweise"
        self.add_attribute("Abstammung", abstammung)


def extract_paragraph(text):
    lines = text.split("\n")
    paragraphs = []
    for line in lines:
        if line.endswith(":"):
            paragraphs.append([line])
        else:
            paragraphs[-1].append(line)
    result = {}
    for p in paragraphs:
        result[p[0][:-1]] = p[1:]
    return result


def ja_nein(text):
    if "j" in text.lower():
        return True
    elif "n" in text.lower():
        return False
    else:
        return None