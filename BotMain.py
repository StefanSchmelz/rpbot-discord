#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import RpBot
import os
from dotenv import load_dotenv


def main(**kwargs):
    load_dotenv()
    token = os.getenv("DISCORD_TOKEN")
    client = RpBot.RpBot()
    client.run(token)


if __name__ == "__main__":
    main()