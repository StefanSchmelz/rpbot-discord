#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def print_map(data, indent, width):
    result = []
    if type(data) is dict:
        for key in data.keys():
            result.append((" " * indent) + "**" + key + "**:")
            if data[key]:
                result.extend(print_map(data[key], indent + width, width))
    elif type(data) is list:
        for line in data:
            result.append((" " * indent) + line)
    return result


def main(**kwargs):
    pass


if __name__ == "__main__":
    main()