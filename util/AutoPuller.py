#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests
import dotenv
import gitlab
import git
import os
import time


def main(**kwargs):
    dotenv.load_dotenv()
    gl = gitlab.Gitlab('http://gitlab.com', private_token=os.getenv("GITLAB_TOKEN"))
    repo = git.Repo(".")
    last_id = git.Repo(".").commit("HEAD")
    while True:
        if gl.projects.list(owned=True, search="RpBot")[0].commits.list()[0].id != repo.commit("HEAD").hexsha:
            print("Detected Newer commit! Pulling ...")
            repo.remotes.origin.pull()
        else:
            time.sleep(300)



if __name__ == "__main__":
    main()